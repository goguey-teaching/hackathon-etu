# Hackathon
Let's get coding!

Pour commencer :
1. Dupliquez le fichier `js/players/DummyTeam.js`, et changer son nom avec votre nom d'équipe (par exemple `MonEquipe.js`).
2. Dans le fichier que vous venez de créer, changer la valeur du champs `name: 'DummyTeam',`
avec votre nom d'équipe (par exemple `name: 'Ma super équipe',`).
3. Toujours dans ce fichier, changer le nom de la viariable `var DummyTeam` avec votre nom de fichier (par exemple `var MonEquipe`).
4. Dans le fichier ```index.html```, ajoutez votre fichier équipe (c'est le fichier que vous avez créez au début) en suivant l'exemple suivant:
```html
        <!----------------------- ADD YOUR TEAM HERE ----------------------->
        <script type="text/javascript" src="js/players/MonEquipe.js"></script>
        <script type="text/javascript" src="js/players/DummyTeam.js"></script>
        <!------------------------------------------------------------------>
```
5. Dans le fichier `js/main.js`, ajoutez votre équipe (c'est la variable que vous avez rennomé) en suivant l'exemple suivant:
```js
        // ADD Your team here
        Hackathon.players.push(MonEquipe);
        Hackathon.players.push(DummyTeam);
        //////////////////////
```

### Comment je fais pour m'entrainer dans un jeu à deux joueurs ?
C'est simple ! Il suffit de créer une deuxième équipe et d'y copier votre code.
Vous vous entrainerez contre vous-même.

### C'est tout prêt !
Maintenant tous ce pass dans le fichier javascript que vous avez créé.

**Happy Coding!**