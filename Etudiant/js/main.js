var backend = null;

function sleep(ms) {
    return new Promise( resolve => setTimeout(resolve, ms) );
}

function update() {
    // Called when settings changed
    Sudoku.difficulty = DEFAULT_SETTINGS['Sudoku difficulty'].val;
    
    Battleship.size = Math.round(DEFAULT_SETTINGS['Battleship size']);
    Battleship.boats = DEFAULT_SETTINGS['Battleship boats'].split(',').map(x => 1 | parseInt(x));

    Maze.size = Math.round(DEFAULT_SETTINGS['Maze size']);
}

document.getElementById('game').addEventListener('change', function(evt) {
    Hackathon.set_game(evt.target.value);
    if (evt.target.value != '') {
        document.getElementById('start').removeAttribute('disabled');
    } else {
        document.getElementById('start').setAttribute('disabled', 'true');
    }
})

document.getElementById('start').addEventListener('click', function(evt) {
    document.getElementById('game').setAttribute('disabled', 'true');
    document.getElementById('start').setAttribute('disabled', 'true');
    Hackathon.start_game();
})
document.getElementById('start').setAttribute('disabled', 'true');


// General Initialization
backend = new BackEnd(DEFAULT_SETTINGS, update);
backend.init();

Hackathon.init();
window.onresize = function() {
    Hackathon.update();
}

// ADD Your team here
Hackathon.players.push(HackTeam);
Hackathon.players.push(NumeroBis);
Hackathon.players.push(DummyTeam);
//////////////////////

for (let k = 0; k < Hackathon.players.length; k++) {
    document.getElementById('teams').innerHTML += `${Hackathon.players[k].name}<br>`;
}